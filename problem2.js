/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

var fs = require("fs");

function problem2() {
    fs.readFile("./lipsum.txt", 'utf8', (err, data) => {
        if (err) {
            console.error(err);
        } else {
            console.log(data)

            fs.writeFile("./newLipsum.txt", data.toUpperCase(), "utf8", (err) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log('written file sussefully')

                    fs.readFile("./newLipsum.txt", 'utf8', (err, data) => {
                        if (err) {
                            console.error(err)
                        } else {
                            const dataLowerCase = data.toLowerCase().split('.').join("");
                            console.log(dataLowerCase)

                            fs.writeFile("./newLipsumLowerCase.txt", dataLowerCase, "utf8", (err) => {
                                if (err) {
                                    console.error(err);
                                } else {
                                    console.log('converted to LowerCase and Splited into sentences')

                                    fs.readFile("./newLipsumLowerCase.txt", "utf8", (err, data) => {
                                        if (err) {
                                            console.log(err)
                                        } else {
                                            console.log(data)
                                            const sortedData = JSON.stringify(data.split('').sort().join(" "));
                                            console.log(sortedData);

                                            fs.writeFile("./sortedLipsum.txt",sortedData,"utf8",(err)=>{
                                                if(err){
                                                    console.error(err);
                                                }else{
                                                    console.log("succefully sorted the data");

                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

problem2();

module.exports=problem2