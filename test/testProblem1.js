const problem1 = require("../problem1")
var fs = require("fs")

function generateRandomFiles(directoryName) {

    let fileName = 'genratedFile' + Math.floor(Math.random() * 10)
    console.log(fileName)

    return (fs.writeFile(`./${directoryName}/${fileName}.json`, JSON.stringify({}), (err) => {
        if (err) {
            throw err;
        } else {
            console.log(`File ${fileName} has been saved!`);
            console.log("filesDelitingProcess");
            deletefiles(directoryName, fileName);

        }
    }));
};

function deletefiles(directoryName, filename) {
    fs.unlink(`${directoryName}/${filename}.json`, (err) => {
        if (err) {
            throw err;
        }
        console.log(`${filename} was deleted`);
    });
};



problem1('sri', generateRandomFiles);