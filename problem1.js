/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


var fs = require("fs");

function problem1(directoryName, callbackFunction) {
    fs.mkdir(("./", directoryName), { recursive: true }, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log('Directory created successfully!');
            for (let index = 0; index < 3; index++) {
                callbackFunction(directoryName);
            }
        }
    });
};


module.exports = problem1;